# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.gsub!(/[^A-Z]/, "")
end


# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)

  if str.length.even?
    return str[str.length/2 - 1..str.length/2]
  end
  if str.length.odd?
    return str[str.length/2]
  end

end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count =0
  str.chars.each do |i|
    if VOWELS.include?(i)
      count +=1
    end
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
fact = 1
(1..num).each {|i| fact *= i}
fact
end



# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  # arr.to_s.delete(" [],")
  join = ""

   arr.each_index do |i|
    join += arr[i]
    join += separator unless i == arr.length - 1 #don't add the separator to the end
  end

  join
end


# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
new_arr = []
  str.split("").each_index do |idx|
    if idx.even?
      new_arr << str[idx].downcase
    else
      new_arr << str[idx].upcase
    end
  end
  new_arr.join("")
end


# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
new_arr = []
  str.split.each do |word|
    if word.length >= 5
      word.reverse!
    end
    new_arr << word
  end
new_arr.join(" ")
end


# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
new_arr = []
  (1..n).each do |i|
  if i % 3 == 0 && i % 5 ==0
    new_arr << "fizzbuzz"
  elsif i % 5 == 0
      new_arr << "buzz"
    elsif  i % 3 == 0
        new_arr << "fizz"
    else
      new_arr << i
    end
  end
new_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
new_arr = []
new_arr += arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
    (2..num -1).each do |i|
      if num % i == 0
        return false
      end
    end
    true
end


# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  sorted_array= []
  (1..num).each do |i|
    if num % i == 0
      sorted_array << i
    end
  end
sorted_array
end


# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
factors_array = factors(num)
prime_factors_arr = []
factors_array.each do |i|

if prime?(i) == true
  prime_factors_arr << i
end

end
prime_factors_arr
end


# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
odd_arr = []
even_arr = []
  arr.each do |i|
    if i.odd?
      odd_arr << i
    elsif i.even?
      even_arr << i
    end
  end
  if odd_arr.length > even_arr.length
    return even_arr.join.to_i
  else
    return odd_arr.join.to_i
  end
end
